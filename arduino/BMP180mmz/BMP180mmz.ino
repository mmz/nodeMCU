#include <Wire.h>
#include <Adafruit_BMP085.h>

/*************************************************** 
  This is an example for the BMP085/BMP180 Barometric Pressure & Temp Sensor

  Designed specifically to work with the Adafruit BMP085 Breakout 
  ----> https://www.adafruit.com/products/391

  These displays use I2C to communicate, 2 pins are required to  interface
  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

// Connect VCC of the BMP180 sensor to 3.3V (NOT 5.0V!)
// Connect GND to Ground
// Connect SDA to D3
// Connect SCL to D4


#define XLED D0      // RED-LED connected to D0 (GPIO16)  
  
Adafruit_BMP085 bmp;
  
void setup() {
  Serial.begin(115200);
#if defined(ESP8266)
  Wire.begin(D3, D4);  // SDA, SCL      
#endif
  Serial.println("");
  Serial.println("BMP180 test...");
  pinMode(XLED, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  digitalWrite(XLED, LOW);  // Turn the LED off by making the voltage LOW
  delay(5000);
  Serial.println("");
  if (!bmp.begin()) {
	Serial.println("Could not find a valid BMP085/BMP180 sensor, check wiring!");
	while (1) {}
  }
}
  
void loop() {
  
    digitalWrite(XLED, HIGH);   // Turn the LED on                                 
    
    Serial.print("Temperature = ");
    Serial.print(bmp.readTemperature());
    Serial.println(" *C");
    
    Serial.print("Pressure = ");
    Serial.print(bmp.readPressure());
    Serial.println(" Pa");
    
    // Calculate altitude assuming 'standard' barometric
    // pressure of 1013.25 millibar = 101325 Pascal
    Serial.print("Altitude = ");
    Serial.print(bmp.readAltitude());
    Serial.println(" meters");

    Serial.print("Pressure at sealevel (calculated) = ");
    Serial.print(bmp.readSealevelPressure());
    Serial.println(" Pa");

  // you can get a more precise measurement of altitude
  // if you know the current sea level pressure which will
  // vary with weather and such. If it is 1015 millibars
  // that is equal to 101500 Pascals.
    Serial.print("Real altitude = ");
    Serial.print(bmp.readAltitude(102400)); // Actual sea level pressure
    Serial.println(" meters");
    Serial.println();
    delay(900);
    digitalWrite(XLED, LOW);  // Turn the LED off  
    delay(4000);
}


