/*
 ESP8266 Blink by Simon Peter
 Blink the blue LED on the ESP-01 module
 This example code is in the public domain
 
 The blue LED on the ESP-01 module is connected to GPIO1 
 (which is also the TXD pin; so we cannot use Serial.print() at the same time)
 
 Note that this sketch uses LED_BUILTIN to find the pin with the internal LED
*/

#define LED_BUILTIN D4      // D4=GPIO2
#define LED_FLASH_TIME 250

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  Serial.begin(115200); 
  Serial.println("");
  Serial.println("Blink_mmz");
}

// the loop function runs over and over again forever
void loop() {
  flashLed(5);
  delay(3000);
}

int flashLed( int ntimes) {
  for (int l = 0; l < ntimes; l++)
  {
    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level                                 
    delay(LED_FLASH_TIME);            // Wait for a while ...
    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
    delay(LED_FLASH_TIME);            // Wait for two seconds (to demonstrate the active low LED)
  }
}
