#include <SoftwareSerial.h>

SoftwareSerial swSer(14, 12, false, 256);

void setup() {
  Serial.begin(9600);
  swSer.begin(115200);

  Serial.println("\nSoftware serial test started");

  for (char ch = ' '; ch <= 'z'; ch++) {
    swSer.write(ch);
  }
  swSer.println("");
  Serial.println("\nTP1");
}

void loop() {
  Serial.println("\nTP2");
  while (swSer.available() > 0) {
    Serial.println("\nTP2.2");
    Serial.write(swSer.read());
  }
  while (Serial.available() > 0) {
    Serial.println("\nTP2.3");
    swSer.write(Serial.read());
  }
  Serial.println("\nTP3");
}
