/*
 ESP32 Blink by M. Zilker
 Set the the blue LED ON/OFF on the ESP32 module
 
 The blue LED on the ESP-32 WROOM-32 module is connected to GPIO1 
 which is also the TXD pin for serial communication with the host.
 Therefore you cannot use Serial.print() at the same time when you 
 want the LED blinking in exactly the timing you defined.
 When a message is transmiited over Serial.print() the LED is ON
 and interferes with your own code.
*/

/* Internal "Blue LED" is connected to GPIO1 which is also TX0 for serial comm. with host */
#define BLUE_LED_BUILTIN 1   // GPIO 01      

#define LED_FLASH_TIME 500   // LED ON/OFF time is ~500 ms

void setup() {
  pinMode(BLUE_LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  //Serial.begin(9600);             // To see the LED blinking with our code, turn off Serial comm.
}

// the loop function runs over and over again forever
void loop() {
  flashLed(5);  // Blink LED 5 times
  delay(2000);  // Make a break for 2 seconds where LED is OFF
}

int flashLed( int ntimes) {
  for (int loops = 0; loops < ntimes; loops++)
  {
    Serial.println("Led ON");              // Print a message to the host over Serial connection
    digitalWrite(BLUE_LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level                                 
    delay(LED_FLASH_TIME);                 // Wait for LED_FLASH_TIME
    Serial.println("Led OFF");             // Print a message to the host over Serial connection 
    digitalWrite(BLUE_LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
    delay(LED_FLASH_TIME);                 // Wait for LED_FLASH_TIME
  }
}
