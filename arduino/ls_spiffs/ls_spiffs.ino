/* 
 * 
List the contents of a SPIFFS file
*
*/

#include"FS.h"

String file_name = "/config.json"; // File of interest

void setup() {
  Serial.begin(115200);
  bool exist;
  bool ok = SPIFFS.begin();
  if (ok) {
    Serial.println("ok - spiffs is mounted");
    Serial.println("File list of directory ...");
    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {
        Serial.print("Filename: ");
        Serial.print(dir.fileName());
        File f = dir.openFile("r");
        Serial.print(" - Filesize: ");
        Serial.print(f.size());
        Serial.println(" Bytes");
    }

    exist = SPIFFS.exists(file_name);

    if (exist) {
      Serial.print("The file /config.json exists!");

      File f = SPIFFS.open(file_name, "r");
      if (!f) {
        Serial.println("Some thing went wrong trying to open the file...");
      }
      else {
        int s = f.size();
        Serial.printf(" - Size=%d Bytes\r\n", s);


// USE THIS DATA VARIABLE

        String data = f.readString();
        Serial.println(data);

        f.close();
      }
    }
    else {
      Serial.println("No such file found.");
    }
  }
}


void loop() {
  // put your main code here, to run repeatedly:
  //Serial.print("ls_spiffs ... ");
    delay(15000);
}


