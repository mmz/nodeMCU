-- Let on board LED blink
LED_PIN = 4
US_TO_MS = 1000
MAX_LOOPS = 100
gpio.mode(LED_PIN, gpio.OUTPUT)

for loop=1, MAX_LOOPS, 1 do
   gpio.write(LED_PIN, gpio.HIGH)
   tmr.delay(500 * US_TO_MS)
   gpio.write(LED_PIN, gpio.LOW)
   tmr.delay(500 * US_TO_MS)
end
-- always clear LED (off) when program ends
gpio.write(LED_PIN, gpio.HIGH) 

