-- Show available Wifi access points 5 times
MAX_LOOPS = 5
wifi.setmode(wifi.STATION)
function listap(t)
for loop=1, MAX_LOOPS, 1 do
   print("loop:"..loop)
   for k,v in pairs(t) do
      print(k.." : "..v)
   end
   tmr.delay(500 * 10000) 
end
print("Wifi-Scan terminated")
end
wifi.sta.getap(listap)
