Readme for mmz GitLab Repo "nodeMCU" 
The repository contains: 
Software (lua+arduino avm cc)

Docs:
https://nodemcu.readthedocs.io/en/master/getting-started/

Firmware:

Actual Custom build: https://nodemcu-build.com/index.php
Old firmware version (floating point, 2016)
https://github.com/nodemcu/nodemcu-firmware/releases/download/0.9.6-dev_20150704/nodemcu_float_0.9.6-dev_20150704.bin

*** NEW FLASHING TOOL *** NEW FLASHING TOOL *** NEW FLASHING TOOL ***
I invite you to try the new GUI tool https://github.com/marcelstoer/nodemcu-pyflasher

Learn how to flash the firmware: https://nodemcu.readthedocs.io/en/master/en/flash/. 

Silicon Labs CP210x Serial driver installation (for ESP8266 modules serial <--> USB communication)
https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers

Schematics & Docs:
nodeMCU-Amica version 1.0  https://github.com/nodemcu/nodemcu-devkit-v1.0

Useful info & hints ...

Links:
http://learn.acrobotic.com/tutorials/post/esp8266-getting-started
http://www.averagemanvsraspberrypi.com/2015/11/esp8266-node-mcu-setup.html
http://www.electrodragon.com/w/ESP8266_NodeMCU_Dev_Board
http://maxtechtv.de/esp8266-part1-de/
http://www.nodemcu.com/index_en.html#fr_5475f7667976d8501100000f

Arduino IDE 1.8.3 (actual version from 08/2017) 
https://www.arduino.cc/en/Main/Software

ESP8266 nodeMCU Board-Package for Arduino
http://arduino.esp8266.com/stable/package_esp8266com_index.json
Sources for ESP8266 Board Package:
https://github.com/esp8266/Arduino

ESPExeptionDecoder (both for ESP8266 & ESP32)
https://github.com/me-no-dev/EspExceptionDecoder

TimeLib.h (for ESP8266)
https://www.arduinoclub.de/2016/05/07/arduino-ide-esp8266-ntp-server-timezone/

ESP8266 Webserver tutorial
http://www.instructables.com/id/Programming-a-HTTP-Server-on-ESP-8266-12E/

ESP8266/32 SPIFFS
http://www.instructables.com/id/Using-ESP8266-SPIFFS/

Löschen von SPIFFS-Konfiguration mit ESP8266FS
https://github.com/esp8266/arduino-esp8266fs-plugin/releases/download/0.3.0/ESP8266FS-0.3.0.zip
Store esp8266fs.jar in dir ~/../Arduino/tools/ESP8266FS/tool/esp8266fs.jar and restart Arduino

Arduino-nodeMCU-Tutorial
http://www.mikrocontroller-elektronik.de/nodemcu-esp8266-tutorial-wlan-board-arduino-ide/
https://alexbloggt.com/nodemcu-einfuehrung/SSD1306 OLED Display
http://randomnerdtutorials.com/esp8266-0-96-inch-oled-display-with-arduino-ide/
https://github.com/esp8266/Arduino/issues/584

Arduino ESP8266 Core & Libraries location (example)
C:\Users\mmz\AppData\Local\Arduino15\packages\esp8266\hardware\esp8266\2.3.0\

Note:
Wenn ihr ueber die Arduino-IDE einen Sketch auf das Board uebertragt, muesst ihr, um es wieder mit Lua programmieren zu koennen 
die NodeMCU-Firmware erneut flashen.

NodeMCU-ESP12E-IO-mapping:

   I/O pin  GPIO Pin    
#define D0  16
#define D1  5       // I2C Bus SCL (clock)
#define D2  4       // I2C Bus SDA (data)
#define D3  0
#define D4  2       // Same as "LED_BUILTIN", but inverted logic
#define D5  14      // SPI Bus SCK (clock)
#define D6  12      // SPI Bus MISO 
#define D7  13      // SPI Bus MOSI
#define D8  15      // SPI Bus SS (CS)
#define D9  3       // RX0 (Serial console)
#define D10 1       // TX0 (Serial console)

#define D11 9
#define D12 10


SPI Slave/Master  
Connect the SPI Master/Slave device to the following pins on the esp8266:

GPIO   NodeMCU   Name     
=======================
15       D8      SS      
13       D7      MOSI     
12       D6      MISO     
14       D5      SCK      

Note: If the ESP is booting at a moment when the SPI Master has the Select line HIGH (deselected)
the ESP8266 WILL FAIL to boot!
See SPISlave_SafeMaster example for possible workaround

/*** ESP32 ***/
General Info
esp32.net

Tutorial
https://www.elektormagazine.de/news/mein-weg-in-das-iot-17-aktor-board-mit-esp32
https://gojimmypi.blogspot.de/2017/03/jtag-debugging-for-esp32.html
https://learn.sparkfun.com/tutorials/esp32-thing-hookup-guide
https://www.cnx-software.com/2017/05/07/getting-started-with-esp32-bit-module-and-esp32-t-development-board-using-arduino-core-for-esp32/
http://blob.tomerweller.com/esp32-first-steps

ESP32 nodeMCU Board-Package for Arduino
https://nzmtinker.wordpress.com/2017/02/24/arduino-ide-installieren-und-konfigurieren/

ESP32 Arduino @Github
https://github.com/espressif/arduino-esp32.git
https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-ledc.h
https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-sigmadelta.h
https://github.com/espressif/arduino-esp32/blob/master/cores/esp32/esp32-hal-dac.h

ESP32 gpio
https://github.com/espressif/esp-idf/blob/master/components/driver/include/driver/gpio.h

ESP32 SoftwareSerial
https://github.com/jdollar/espsoftwareserial/
https://github.com/plerup/espsoftwareserial
https://www.reddit.com/r/hackerboxes/comments/5xba6g/softwareserial_working_for_esp32arduino_ide/
https://hackaday.com/2017/08/17/secret-serial-port-for-arduinoesp32/

ESP32 ARDUINO Pin mapping

Note: For the ESP32 pin numbers (labels) correspond 1:1 to GPIO numbers.
Input Only Pins: 34-39

Pins 34, 35, 36, 37, 38 and 39 cannot be configured as outputs, but they can be used as either 
digital inputs, analog inputs, or for other unique purposes. 
Also note that they do not have internal pull-up or pull-down resistors, like the other I/O pins.

GPIO pins 36-39 are an integral part of the ultra low noise pre-amplifier for the ADC and 
they are wired up to 270pF capacitors, which help to configure the sampling time and noise of the pre-amp.

https://github.com/espressif/arduino-esp32/blob/master/variants/nodemcu-32s/pins_arduino.h

#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include <stdint.h>

#define EXTERNAL_NUM_INTERRUPTS 16
#define NUM_DIGITAL_PINS        40
#define NUM_ANALOG_INPUTS       16

#define analogInputToDigitalPin(p)  (((p)<20)?(esp32_adc2gpio[(p)]):-1)
#define digitalPinToInterrupt(p)    (((p)<40)?(p):-1)
#define digitalPinHasPWM(p)         (p < 34)

static const uint8_t LED_BUILTIN = 2;
#define BUILTIN_LED  LED_BUILTIN // backward compatibility

static const uint8_t KEY_BUILTIN = 0;

static const uint8_t TX = 1;
static const uint8_t RX = 3;

static const uint8_t SDA = 21;
static const uint8_t SCL = 22;

static const uint8_t SS    = 5;
static const uint8_t MOSI  = 23;
static const uint8_t MISO  = 19;
static const uint8_t SCK   = 18;

static const uint8_t A0 = 36;
static const uint8_t A3 = 39;
static const uint8_t A4 = 32;
static const uint8_t A5 = 33;
static const uint8_t A6 = 34;
static const uint8_t A7 = 35;
static const uint8_t A10 = 4;
static const uint8_t A11 = 0;
static const uint8_t A12 = 2;
static const uint8_t A13 = 15;
static const uint8_t A14 = 13;
static const uint8_t A15 = 12;
static const uint8_t A16 = 14;
static const uint8_t A17 = 27;
static const uint8_t A18 = 25;
static const uint8_t A19 = 26;

static const uint8_t T0 = 4;
static const uint8_t T1 = 0;
static const uint8_t T2 = 2;
static const uint8_t T3 = 15;
static const uint8_t T4 = 13;
static const uint8_t T5 = 12;
static const uint8_t T6 = 14;
static const uint8_t T7 = 27;
static const uint8_t T8 = 33;
static const uint8_t T9 = 32;

static const uint8_t DAC1 = 25;
static const uint8_t DAC2 = 26;

#endif /* Pins_Arduino_h */
